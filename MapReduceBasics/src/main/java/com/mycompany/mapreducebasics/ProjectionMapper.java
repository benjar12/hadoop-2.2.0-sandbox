/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mapreducebasics;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapred.OutputCollector;

public class ProjectionMapper extends Mapper<LongWritable, Text, Text, LongWritable> {

    private Text word = new Text();
    private final static IntWritable one = new IntWritable(1);

    //@Override
    protected void map(LongWritable key, Text value, Context context, OutputCollector<Text, IntWritable> output) throws IOException {

        String line = value.toString();
        StringTokenizer tokenIzr = new StringTokenizer(line);
        while (tokenIzr.hasMoreTokens()){
            word.set(tokenIzr.nextToken());
            output.collect(word, one);
        }
    }
}
